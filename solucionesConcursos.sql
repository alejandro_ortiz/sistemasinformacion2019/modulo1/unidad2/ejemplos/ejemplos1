﻿/**
  Ejemplo 1
  Consultas de seleccion
  Son un conjunto de consultas con totales y seleccion
  Vamos a trabajar con where, havin y group by
*/

USE concursos;

/*
  1. Indicar el nombre de los concursantes que son de burgos 
*/

  SELECT 
    DISTINCT c.nombre 
  FROM 
    concursantes c 
  WHERE 
    c.provincia='Burgos';

  -- Solución con LIKE
  SELECT 
    DISTINCT c.nombre 
  FROM 
    concursantes c 
  WHERE 
    c.provincia LIKE 'Burgos';

/*
  2. Indicar cuantos concursantes hay de burgos
*/
  SELECT 
    COUNT(*)concursantes_burgos 
  FROM 
    concursantes c 
  WHERE 
    c.provincia='Burgos';

/*
  3. Indicar cuantos concursantes hay de cada población
*/

  SELECT 
    c.poblacion, COUNT(*)numero_concursantes 
  FROM 
    concursantes c 
  GROUP BY 
    c.poblacion;

/*
  4. Indicar las poblaciones que tienen concursantes de menos de 90kg
*/
  
  SELECT 
    DISTINCT c.poblacion 
  FROM 
    concursantes c 
  WHERE 
    c.peso<90;
  
/*
  5. Indicar las poblaciones que tienen más de 1 concursante
*/

  SELECT 
    c.poblacion, COUNT(*)numero_concursantes
  FROM 
    concursantes c 
  GROUP BY 
    c.poblacion 
  HAVING 
    numero_concursantes>1;

  -- Solo mostrando la poblacion
  SELECT 
    c.poblacion
  FROM 
    concursantes c 
  GROUP BY 
    c.poblacion 
  HAVING 
    COUNT(*)>1;

/*
  6. Indicar las poblaciones que tienen más de 1 concursante de menos de 90kg
*/

  SELECT 
    c.poblacion, COUNT(*)numero_concursantes 
  FROM 
    concursantes c 
  WHERE 
    c.peso<90 
  GROUP BY 
    c.poblacion 
  HAVING 
    numero_concursantes>1;

  -- Solo mostrando la poblacion
  SELECT 
    c.poblacion 
  FROM 
    concursantes c 
  WHERE 
    c.peso<90 
  GROUP BY 
    c.poblacion 
  HAVING 
    COUNT(*)>1;

